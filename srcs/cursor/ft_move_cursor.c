/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_cursor.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 13:49:51 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/21 15:27:28 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cursor.h"
#include "ft_printf.h"

static void	ft_cursor_resize(t_cursor *cursor)
{
	int	d;

	d = (cursor->l * cursor->r_with) + cursor->r;
	cursor->l = d / cursor->r_with;
	cursor->r = d % cursor->r_with;
}

static void	ft_move_cursor_right(t_cursor *cur)
{
	t_listd	*node;
	int		i;

	if (cur->l == 0 && cur->r >= cur->r_with)
	{
		cur->l = cur->l + 1;
		cur->r = 0;
	}
	else if (cur->r >= cur->r_with - 1 && (cur->l = cur->l + 1))
		cur->r = 0;
	else
	{
		node = cur->lst;
		i = cur->id_list;
		while (node && --i > 0)
			node = node->next;
		if (node && node->c == '\n')
		{
			cur->r = 0;
			cur->l = cur->l + 1;
		}
		else
			cur->r = cur->r + 1;
	}
}

static void	ft_move_cursor_left(t_cursor *cr)
{
	t_listd	*node;
	int		i;

	node = cr->lst;
	i = cr->id_list;
	while (node && i-- > 0)
		node = node->next;
	if (node && node->c == '\n' && (i = 0) == 0)
	{
		node = node->prev;
		if (cr->l > 0)
			cr->l = cr->l - 1;
		while (node && node->c != '\n' && ++i)
			node = node->prev;
		cr->r = i;
		if (cr->l == 0)
			cr->r = cr->r + cr->l_pt;
		return ;
	}
	if (cr->r == 0)
	{
		cr->l = cr->l - 1;
		cr->r = cr->r_with;
	}
	else
		cr->r = cr->r - 1;
}

int			ft_move_cursor_multi(int move)
{
	t_cursor	*curs;
	int			d;

	curs = get_cursor();
	if (move == UP)
	{
		curs->l = curs->l - 1;
		if (curs->l == 0 && curs->id_list < curs->r_with)
		{
			curs->r = curs->l_pt;
			curs->id_list = 0;
		}
		else
			curs->id_list = (curs->l * curs->r_with) + curs->r - curs->l_pt;
	}
	else if (move == DOWN)
	{
		curs->l = curs->l + 1;
		d = (curs->l * curs->r_with) + curs->r;
		if (curs->r > curs->l_list % curs->r_with + curs->l_pt)
			curs->r = curs->l_list % curs->r_with + curs->l_pt;
		curs->id_list = (curs->l * curs->r_with) + curs->r - curs->l_pt;
	}
	return (1);
}

int			ft_move_cursor(int move)
{
	t_cursor *cur;

	cur = get_cursor();
	if (move == NOP)
		ft_cursor_resize(cur);
	else if (move == LEFT)
	{
		ft_move_cursor_left(cur);
	}
	else if (move == RIGHT)
		ft_move_cursor_right(cur);
	return (1);
}
