/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_process.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 15:20:03 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/21 10:19:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getinput.h"

int			ft_process(t_cursor *cr, char *s, int rt)
{
	int		i;

	i = 0;
	ft_dprintf(9, "key pressed:\n");
	while (i < rt)
		ft_dprintf(9, "%.2hhx", s[i++]);
	ft_dprintf(9, "\ndone\n");
	if (rt == 1)
		return (ft_process_ascii(cr, *s));
	else
	{
		ft_process_key(cr, s);
		return (0);
	}
}
