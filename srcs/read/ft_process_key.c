/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_process_key.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 15:56:30 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/21 16:28:43 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getinput.h"

int		ft_ishome_end(t_cursor *cr, t_ks *ks, char *s)
{
	if (strcmp(s, ks->home) == 0 && cr->l_list > 0)
	{
		cr->r = cr->l_pt;
		cr->l = 0;
		cr->id_list = 0;
		return (1);
	}
	else if (strcmp(s, ks->end) == 0 && cr->l_list > 0)
	{
		cr->id_list = 0;
		cr->r = cr->l_pt;
		cr->l = 0;
		while (cr->id_list < cr->l_list)
		{
			cr->id_list = cr->id_list + 1;
			ft_move_cursor(RIGHT);
		}
		return (1);
	}
	return (0);
}

int		ft_isarrow_key(t_cursor *cr, char *s, t_ks *ks)
{
	(void)cr;
	int		mode;

	mode = 0;
	if (strcmp(s, ks->down_a) == 0)
		mode = DOWN;
	else if (strcmp(s, ks->upper_a) == 0)
		mode = UP;
	else if (strcmp(s, ks->right_a) == 0)
		mode = RIGHT;
	else if (strcmp(s, ks->left_a) == 0)
		mode = LEFT;
	return (mode);
}

int		ft_ispage(t_cursor *cr, t_ks *ks, char *s)
{
	if (strcmp(ks->p_up, s) == 0 && cr->l > 0)
	{
		ft_move_cursor_multi(UP);
		return (1);
	}
	if (strcmp(ks->p_do, s) == 0 && cr->l < (cr->l_list + cr->l_pt) / cr->r_with)
	{
		ft_move_cursor_multi(DOWN);
		return (1);
	}
	return (0);
}

int		ft_isstep_word(t_cursor *cr, t_ks *ks, char *s)
{
	if (strcmp(s, ks->sil) == 0)
	{
		ft_step_word(LEFT, cr);
		return (1);
	}
	if (strcmp(s, ks->sir) == 0)
	{
		ft_step_word(RIGHT, cr);
		return (1);
	}
	return (0);
}

int		ft_isdel_key(t_cursor *cr, t_ks *ks, char *s)
{
	if (strcmp(s, ks->del) == 0 && cr->id_list < cr->l_list)
	{
		cr->id_hist = -1;
		ft_listd_remove_at(&cr->lst, cr->id_list + 1);
		cr->l_list = cr->l_list - 1;
		return (0);
	}
	return (0);
}

int		ft_process_key(t_cursor *cr, char *s)
{
	t_ks	*ks;
	int		md;

	ks = get_ks();
	if ((md = ft_isarrow_key(cr, s, ks)))
	{
		if (md == LEFT || md == RIGHT)
			return (ft_process_cursor(cr, md));
		else
			return (ft_process_hist(cr, md));
	}
	if ((md = ft_ishome_end(cr, ks, s)))
		return (md);
	if ((md = ft_ispage(cr, ks, s)))
		return (md);
	if ((md = ft_isstep_word(cr, ks, s)))
		return (md);
	if ((md = ft_isdel_key(cr, ks, s)))
		return (md);
	ft_setterm("bl");
	return (1);
}
