/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 15:04:37 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/22 09:28:52 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getinput.h"

void		ft_save_np(int count)
{
	char	*s;

	ft_dprintf(9, "save np:%d\n", count);
	if (count == 0)
		return ;
	ft_setterm("cr");
	s = tgetstr("UP", NULL);
	tputs(tgoto(s, 0, count), 0, ft_putchar_ent);
	ft_setterm("sc");
}

int		ft_write(int fd, char *s, int max, int len)
{
	char	*t;
	int		count;

	ft_dprintf(9, "s>{\n%s\n}", s);
	if (len < max)
		max = len;
	if ((t = strchr(s, '\n')) == NULL)
	{
		write(fd, s, max);
		return (0);
	}
	write(fd, s, t - s);
	count = 1;
	ft_setterm("do");
	max -= (t - s) + 1;
	s = t + 1;
	while ((t = strchr(s, '\n')))
	{
		count++;
		write(fd, s, t - s);
		ft_setterm("do");
		// max -= (t - s) + 1;
		s = t + 1;
	}
	write(fd, s, strlen(s));
	return (count);
}

void		display_cmd(char *s, int len, t_cursor *cr)
{
	int	count;
	int	ncount;
	int	ll;

	ncount = 0;
	count = 0;
	if (len + cr->l_pt < cr->r_with)
		ncount = ft_write(STDIN_FILENO, s, len, len);
	else
	{
		count++;
		ll = len;
		ft_write(STDIN_FILENO, s, cr->r_with - cr->l_pt, ll);
		ft_setterm("do");
		while (cr->r_with * count < len + cr->l_pt)
		{
			ncount += ft_write(STDIN_FILENO,\
			s + cr->r_with * count - cr->l_pt, cr->r_with, ll);
			count++;
			ll = len - cr->r_with * count - cr->l_pt;
			ft_setterm("do");
		}
	}
	ft_save_np(count + ncount);
}

void		ft_place_cursor(t_cursor *cursor)
{
	int		row;

	ft_setterm("rc");
	row = cursor->r;
	if (cursor->l > 0)
		tputs(tgoto(tgetstr("DO", 0), 0, cursor->l), 0, ft_putchar_ent);
	if (row > 0)
		tputs(tgoto(tgetstr("RI", 0), 0, row), 0, ft_putchar_ent);
}

void		ft_display(t_cursor *cr)
{
	char	*s;

	ft_setterm("rc");
	ft_setterm("cd");
	s = ft_listd_to_str(&cr->lst);
	write(STDERR_FILENO, cr->pt, cr->l_pt);
	display_cmd(s, cr->l_list, cr);
	free(s);
	ft_place_cursor(cr);
}
