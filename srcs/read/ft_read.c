/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 14:23:37 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/19 15:48:07 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getinput.h"

void	ft_init_cursor(t_cursor *cr, char *pt)
{
	cr->l = 0;
	cr->lst = NULL;
	cr->l_pt = strlen(pt);
	cr->r = cr->l_pt;
	cr->id_list = 0;
	cr->l_list = 0;
	cr->exit = "exit";
	cr->pt = pt;
	ft_setterm("sc");
	handler_resize(0);
}

char	*ft_end_read(t_cursor *cr, int rt)
{
	char	*s;

	if (rt == -1)
	{
		g_errn = ERR_READ;
		return (NULL);
	}
	s = ft_listd_to_str(&cr->lst);
	ft_listd_free(&cr->lst);
	return (s);
}

char	*ft_read(char *pt)
{
	t_cursor	*cr;
	char		s[11];
	int			rt;

	cr = get_cursor();
	ft_init_cursor(cr, pt);
	ft_dprintf(2, "%s", pt);
	while ((rt = read(STDIN_FILENO, s, 11)) > 0)
	{
		ft_dprintf(9, "rt:%d\n", rt);
		s[rt] = 0;
		if (ft_process(cr, s, rt))
			break;
		ft_display(cr);
	}
	return (ft_end_read(cr, rt));
}
