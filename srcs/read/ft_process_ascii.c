/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_process_ascii.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 15:25:28 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/21 11:23:50 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getinput.h"

int			ft_process_char(t_cursor *cr, char c)
{
	cr->l_list = cr->l_list + 1;
	cr->id_list = cr->id_list + 1;
	ft_listd_add_at(&cr->lst, cr->id_list - 1, c);
	ft_move_cursor(RIGHT);
	return (0);
}

int			ft_process_ctrl(t_cursor *cr, char c)
{
	if (c == '\n')
	{
		cr->l_list = cr->l_list + 1;
		cr->id_list = cr->id_list + 1;
		ft_move_cursor(RIGHT);
		ft_listd_add_at(&cr->lst, cr->l_list - 1, c);
		return (1);
	}
	else if (c == 4)
	{
		if (cr->l_list == 0)
		{
			ft_listd_free(&cr->lst);
			cr->l_list = strlen(cr->exit);
			cr->lst = ft_str_to_listd(cr->exit);
			ft_listd_add_at(&cr->lst, cr->l_list, '\n');
			return (1);
		}
		else if (cr->l_list + cr->l_pt > cr->r + (cr->l * cr->r_with))
		{
			cr->l_list = cr->l_list - 1;
			ft_listd_remove_at(&cr->lst, cr->id_list + 1);
		}
	}
	else if (c == 0x7f && cr->l_list > 0 && cr->id_list > 0)
	{
		cr->l_list = cr->l_list - 1;
		cr->id_list = cr->id_list - 1;
		ft_move_cursor(LEFT);
		ft_listd_remove_at(&cr->lst, cr->id_list + 1);
	}
	return (0);
}

int			ft_process_ascii(t_cursor *cr, char c)
{
	cr->id_hist = -1;
	if (c >= ' ' && c < 127)
		return (ft_process_char(cr, c));
	else
		return (ft_process_ctrl(cr, c));
}
