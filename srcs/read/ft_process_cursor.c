/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_process_cursor.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 16:27:07 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/21 09:27:52 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getinput.h"

int		ft_process_cursor(t_cursor *cr, int mode)
{
	if (mode == LEFT && cr->id_list > 0)
	{
		ft_dprintf(9, "move to left\n");
		cr->id_list = cr->id_list - 1;
		ft_move_cursor(LEFT);
	}
	else if (mode == RIGHT && cr->id_list < cr->l_list)
	{
		cr->id_list = cr->id_list + 1;
		ft_move_cursor(RIGHT);
	}
	return (0);
}
