/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_step_word.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/19 14:12:50 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/21 15:25:11 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getinput.h"

static void	ft_move_right_cursor(t_cursor *cr, char c)
{
	if (cr->r + 1 < cr->r_with - 1)
	{
		if (c == '\n')
		{
			cr->r = 0;
			cr->l = cr->l + 1;
		}
		else
			cr->r = cr->r + 1;
	}
	else
	{
		cr->l = cr->l + 1;
		cr->r = 0;
	}
}

static void ft_move_left_cursor(t_cursor *cr, t_listd *node)
{
	int		i;

	if (node && node->c == '\n')
	{
		i = 0;
		node = node->prev;
		if (cr->l > 0)
			cr->l = cr->l - 1;
		while (node && node->c != '\n' && ++i)
			node = node->prev;
		cr->r = i;
		if (cr->l == 0)
			cr->r = cr->r + cr->l_pt;
		return ;
	}
	if (cr->r == 0)
	{
		cr->l = cr->l - 1;
		cr->r = cr->r_with;
	}
	else
		cr->r = cr->r - 1;
}

static void	ft_move_left(t_listd *headref, t_cursor *cr)
{
	t_listd *node;
	int		i;

	node = headref;
	i = -1;
	while (++i < cr->id_list && node)
		node = node->next;
	while (node && !ft_isspace(node->c) && cr->id_list > 0)
	{
		node = node->prev;
		ft_move_left_cursor(cr, node);
		cr->id_list = cr->id_list - 1;
	}
	while (node && ft_isspace(node->c) && cr->id_list > 0)
	{
		node = node->prev;
		cr->id_list = cr->id_list - 1;
		ft_move_left_cursor(cr, node);
	}
	while (node && node->prev && !ft_isspace(node->prev->c) && cr->id_list > 0)
	{
		node = node->prev;
		cr->id_list = cr->id_list - 1;
		ft_move_left_cursor(cr, node);
	}
}

static void	ft_move_right(t_listd *headref, t_cursor *cr)
{
	t_listd *node;
	int		i;

	node = headref;
	i = -1;
	while (++i < cr->id_list && node)
		node = node->next;
	while (node && !ft_isspace(node->c))
	{
		ft_move_right_cursor(cr, node->c);
		node = node->next;
		cr->id_list = cr->id_list + 1;
	}
	while (node && ft_isspace(node->c))
	{
		ft_move_right_cursor(cr, node->c);
		node = node->next;
		cr->id_list = cr->id_list + 1;
	}
}

int			ft_step_word(int move, t_cursor *cr)
{
	t_listd		*headref;
	t_listd		*node;
	t_listd		nut;

	headref = cr->lst;
	if (headref == NULL)
		return (1);
	if (move == LEFT && *index > 0)
	{
		nut.c = ' ';
		node = headref;
		while (node->next)
			node = node->next;
		node->next = &nut;
		nut.prev = node;
		ft_move_left(headref, cr);
		nut.prev->next = NULL;
	}
	if (move == RIGHT && cr->id_list < cr->l_list)
		ft_move_right(headref, cr);
	return (1);
}
