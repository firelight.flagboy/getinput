/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_save_cmd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 15:01:41 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/20 15:44:21 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getinput.h"

int		ft_save_cmd(t_cursor *cr)
{
	if (cr->scmd)
		ft_strdel(&cr->scmd);
	cr->scmd = ft_listd_to_str(&cr->lst);
	return (0);
}

int		ft_reset_cmd(t_cursor *cr)
{
	int			i;

	i = -1;
	ft_dprintf(9, "scmd:%s\n", cr->scmd);
	while (++i < cr->l_list)
		ft_move_cursor(LEFT);
	cr->id_list = 0;
	if (cr->lst)
		ft_listd_free(&cr->lst);
	cr->l_list = 0;
	cr->id_list = 0;
	if (cr->scmd)
	{
		cr->lst = ft_str_to_listd(cr->scmd);
		cr->l_list = ft_strlen(cr->scmd);
		i = -1;
	}
	cr->r = cr->l_pt;
	cr->l = 0;
	cr->id_hist = cr->id_hist - 1;
	return (0);
}
