/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_process_hist.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 14:49:14 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/21 11:48:44 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getinput.h"

int		ft_process_hist(t_cursor *cr, int md)
{
	t_hist **head;

	head = get_history();
	ft_dprintf(9, "idhist:%d\n", cr->id_hist);
	cr->r = cr->l_pt;
	cr->l = 0;
	if (md == UP)
	{
		if (cr->id_hist == -1 && cr->lst)
			ft_save_cmd(cr);
		ft_dprintf(9, "get hist\n");
		ft_get_history(NEXT, cr);
	}
	else if (md == DOWN)
	{
		if (cr->id_hist == 0)
		{
			ft_dprintf(9, "reset cmd\n");
			ft_reset_cmd(cr);
		}
		else
			ft_get_history(PREV, cr);
	}
	ft_dprintf(9, "end hist\n");
	return (0);
}
