/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getinput.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 16:26:20 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/21 16:21:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getinput.h"

void		ft_head_promt(void)
{
	char	buf[PATH_MAX];

	get_cursor()->exit = "exit";
	get_cursor()->id_hist = -1;
	getcwd(buf, PATH_MAX);
	ft_dprintf(2, "\033[2m"
	"-------------------------------------------\033[0m\n");
	ft_dprintf(2, "%s%s\033[0m\n", "\033[32m", buf);
}

char		*ft_loop_until(t_cursor *cr)
{
	int		e;
	char	*tmp;
	char	*res;

	e = 0;
	while (cr->res == NULL || (e = ft_isinc(cr->res)))
	{
		if (e)
			write(STDIN_FILENO, "\n", 1);
		res = NULL;
		res = ft_read(ft_get_promt(e));
		tmp = cr->res;
		ft_dprintf(9, "tmp:%p, res:%p\n", tmp, res);
		if (tmp == NULL)
		{
			cr->res = res;
			continue;
		}
		if (!(cr->res = ft_strjoin(tmp, res)) && ft_eprintf("strjoin error\n"))
			exit(EXIT_FAILURE);
		ft_strdel(&tmp);
		ft_strdel(&res);
		e = 0;
	}
	return (cr->res);
}

int			ft_promt_loop(void)
{
	t_cursor *cr;
	char	s[9];

	cr = get_cursor();
	while (1)
	{
		ft_head_promt();
		if (!(cr->res = ft_read("$> ")))
			ft_eprintf("21sh : %s", ft_strerror(g_errn));
		ft_strdel(&cr->scmd);
		ft_loop_until(cr);
		ft_printf("\nres:%s", cr->res);
		strncpy(s, cr->res, 9);
		if (cr->res[0] != '\n')
			ft_hist_push(get_history(), cr->res);
		ft_strdel(&cr->res);
		if (*s == 0)
			continue;
		if (strcmp(s, "exit\n") == 0)
			break;
	}
	return (0);
}

int		main(void)
{
	char	*nterm;

	if ((!(nterm = getenv("TERM")) && (g_errn = ERR_NOTERM))\
	|| (tgetent(NULL, nterm) == -1 && (g_errn = ERR_GETENT)))
	{
		ft_eprintf("getinput: %s\n", ft_strerror(g_errn));
		return (1);
	}
	if (ft_get_term() == -1)
		exit(EXIT_FAILURE);
	ft_setsignal();
	ft_setterm("cl");
	ft_promt_loop();
	ft_reset_term();
	return (0);
}
