/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 09:52:26 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/21 12:53:26 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char		*res;
	size_t		flen;
	size_t		l1;

	if (s1 == NULL || s2 == NULL)
		return (NULL);
	l1 = strlen(s1);
	flen = l1 + strlen(s2) + 1;
	res = (char*)malloc(flen * sizeof(char));
	if (res == NULL)
		return (NULL);
	strcpy(res, s1);
	strcpy(res + l1, s2);
	return (res);
}
