/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setsignal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 11:38:07 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/20 13:00:56 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "setsignal.h"

void	handler_quit(int sig)
{
	signal(sig, handler_quit);
	exit(EXIT_SUCCESS);
}

void	handler_promt(int sig)
{
	t_cursor	*cursor;

	(void)sig;
	signal(SIGINT, handler_promt);
	cursor = get_cursor();
	ft_setterm("rc");
	ft_move_cursor(RIGHT);
	ft_listd_free(&cursor->lst);
	ft_strdel(&cursor->res);
	ft_place_cursor(cursor);
	cursor->pt = "$> ";
	cursor->l_pt = 3;
	cursor->r = cursor->l_pt;
	cursor->l = 0;
	cursor->l_list = 0;
	cursor->id_list = 0;
	ft_dprintf(2, "\n");
	ft_head_promt();
	ft_setterm("sc");
	write(STDERR_FILENO, cursor->pt, cursor->l_pt);
}

void	handler_pid(int sig)
{
	signal(sig, handler_pid);
	kill(g_pid, sig);
	ft_printf("\n");
	ft_change_term();
}

void	handler_resize(int sig)
{
	t_ttysize	ts;
	t_cursor	*cursor;

	if (sig == SIGWINCH)
	{
		ft_setterm("cl");
		ft_head_promt();
		ft_setterm("sc");
	}
	signal(SIGWINCH, handler_resize);
	ioctl(0, TIOCGSIZE, &ts);
	cursor = get_cursor();
	cursor->r_with = ts.ts_cols - 1;
	cursor->l_with = ts.ts_lines;
	cursor->r = cursor->l_pt;
	cursor->l = 0;
	cursor->id_list = 0;
	if (sig == SIGWINCH)
		ft_display(cursor);
}

int		ft_setsignal(void)
{
	signal(SIGTSTP, SIG_IGN);
	signal(SIGQUIT, handler_quit);
	signal(SIGINT, handler_promt);
	signal(SIGWINCH, handler_resize);
	return (0);
}
