/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd_to_str.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 11:42:24 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 12:16:08 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"

char		*ft_listd_to_str(t_listd **headref)
{
	char	*res;
	t_listd	*node;
	size_t	i;
	size_t	len;

	len = ft_listd_len(*headref);
	if (!(res = (char*)malloc((len + 1) * sizeof(char))))
		return (NULL);
	res[len] = 0;
	i = 0;
	node = *headref;
	while (node)
	{
		res[i++] = node->c;
		node = node->next;
	}
	return (res);
}

char		*ft_listd_to_str_r(t_listd **headref)
{
	char	*res;
	t_listd	*node;
	size_t	i;
	size_t	len;

	len = ft_listd_len(*headref);
	if (!(res = (char*)malloc((len + 1) * sizeof(char))))
		return (NULL);
	res[len] = 0;
	i = 0;
	node = *headref;
	while (node->next)
		node = node->next;
	while (node)
	{
		res[i++] = node->c;
		node = node->prev;
	}
	return (res);
}
