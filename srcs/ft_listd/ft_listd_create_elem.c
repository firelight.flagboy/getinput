/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 15:06:41 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 11:50:31 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"

t_listd		*ft_listd_create_elem(char c)
{
	t_listd	*node;

	if (!(node = (t_listd*)malloc(sizeof(t_listd))))
		return (NULL);
	node->next = NULL;
	node->prev = NULL;
	node->c = c;
	return (node);
}


