/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd_len.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 11:45:40 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 11:46:35 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"

size_t		ft_listd_len(t_listd *head)
{
	size_t	l;

	l = 0;
	while (head)
	{
		head = head->next;
		l++;
	}
	return (l);
}
