/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd_remove_at.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 11:43:42 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 11:43:53 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"

static void	ft_listd_rm_at(t_listd **headref)
{
	t_listd	*node;

	node = *headref;
	if (node->next)
	{
		node = node->next;
		node->prev = NULL;
		free(*headref);
		*headref = node;
	}
	else
	{
		free(node);
		*headref = NULL;
	}
}

void		ft_listd_remove_at(t_listd **headref, ssize_t index)
{
	t_listd	*prev;
	t_listd	*node;

	index--;
	if (*headref == NULL || index < 0)
		return ;
	if (index == 0)
		return (ft_listd_rm_at(headref));
	prev = *headref;
	node = prev->next;
	while (node && --index && (prev = node))
		node = node->next;
	if (index != 0)
		return ;
	prev->next = node->next;
	if (node->next)
		node->next->prev = prev;
	free(node);
}
