/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_to_listd.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 16:10:39 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 11:44:18 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"

t_listd		*ft_str_to_listd(char const *s)
{
	t_listd *head;
	t_listd *node;

	if (*s)
	{
		if (!(head = ft_listd_create_elem(*s)))
			return (NULL);
		s++;
		node = head;
	}
	while (*s)
	{
		if (!(node->next = ft_listd_create_elem(*s)))
			return (NULL);
		node->next->prev = node;
		node = node->next;
		s++;
	}
	return (head);
}
