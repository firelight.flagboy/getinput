/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd_add_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 11:49:07 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 11:49:17 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"

int			ft_listd_add_back(char c, t_listd **headref)
{
	t_listd	*node;
	t_listd	*tmp;

	if (!(node = ft_listd_create_elem(c)))
		return (1);
	if (*headref == NULL)
	{
		*headref = node;
	}
	else
	{
		tmp = *headref;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = node;
		node->prev = tmp;
	}
	return (0);
}
