/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd_add_front.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 11:50:16 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 11:50:26 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"

int			ft_listd_add_front(char c, t_listd **headref)
{
	t_listd	*node;
	t_listd	*tmp;

	if (!(node = ft_listd_create_elem(c)))
		return (1);
	if (*headref == NULL)
	{
		*headref = node;
	}
	else
	{
		tmp = *headref;
		node->next = tmp;
		tmp->prev = node;
		*headref = node;
	}
	return (0);
}
