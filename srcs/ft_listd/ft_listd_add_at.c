/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd_add_at.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 15:10:44 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 11:45:14 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"

int			ft_listd_add_at(t_listd **headref, ssize_t index, char c)
{
	t_listd	*head;
	t_listd	*node;
	ssize_t	i;

	head = *headref;
	if (!(node = ft_listd_create_elem(c)))
		return (1);
	if (head == NULL || index == 0)
	{
		node->next = head;
		*headref = node;
		if (head)
			head->prev = node;
		return (0);
	}
	i = 0;
	while (++i < index && head->next != NULL)
		head = head->next;
	node->next = head->next;
	node->prev = head;
	if (head->next)
		head->next->prev = node;
	head->next = node;
	return (0);
}
