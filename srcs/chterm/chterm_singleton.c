/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chterm_singleton.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 09:33:09 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 10:03:19 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "chterm.h"

t_term	*get_term(void)
{
	static t_term term;

	return (&term);
}

t_ks	*get_ks(void)
{
	static t_ks ks;

	return (&ks);
}
