/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_termcap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 09:51:18 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/19 15:39:08 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "chterm.h"

static int	ft_settermcap(t_ks *ks)
{
	ks->end = "\x1b\x4f\x46";
	ks->opl = "\x1b\x62";
	ks->opr = "\x1b\x66";
	ks->sil = "\x1b\x5b\x31\x3b\x32\x44";
	ks->sir = "\x1b\x5b\x31\x3b\x32\x43";
	return (0);
}

int			ft_get_termcap(void)
{
	char	*res;
	t_ks	*strock;

	if ((res = tgetstr("ks", NULL)) == NULL)
	{
		ft_eprintf("21sh : can't use termcap\n");
		return (-1);
	}
	tputs(res, 0, ft_putchar_ent);
	strock = get_ks();
	if (!(strock->left_a = tgetstr("kl", NULL))\
	|| !(strock->right_a = tgetstr("kr", NULL))\
	|| !(strock->down_a = tgetstr("kd", NULL))\
	|| !(strock->upper_a = tgetstr("ku", NULL))\
	|| !(strock->del = tgetstr("kD", NULL))\
	|| !(strock->home = tgetstr("kh", NULL))\
	|| !(strock->p_up = tgetstr("kP", NULL))\
	|| !(strock->p_do = tgetstr("kN", NULL)))
		return (-1);
	return (ft_settermcap(strock));
}
