/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_term.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 09:27:46 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 13:22:30 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "chterm.h"

int		ft_change_term(void)
{
	t_termios	term;

	term = get_term()->mod;
	term.c_lflag &= ~(ICANON | ECHO);
	term.c_cc[VMIN] = 1;
	term.c_cc[VTIME] = 0;
	if (isatty(0))
		if (tcsetattr(0, TCSADRAIN, &term) == -1)
		{
			ft_eprintf("21sh : can't set the modified"\
			" attribut of the terminal\n");
			return (-1);
		}
	return (0);
}
