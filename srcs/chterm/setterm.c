/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setterm.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 10:08:03 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 10:08:13 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "chterm.h"

void	ft_setterm(char *term)
{
	char	*res;

	res = tgetstr(term, NULL);
	if (res)
		tputs(res, 0, ft_putchar_ent);
}
