/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reset_term.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 10:06:47 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 13:23:01 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "chterm.h"

void	ft_reset_term(void)
{
	char	*res;

	if ((res = tgetstr("ke", NULL)))
		tputs(res, 0, ft_putchar_ent);
	if (isatty(0) == 1 && tcsetattr(0, 0, &(get_term()->off)) == -1)
	{
		ft_eprintf("21sh : error can't reset terminal");
		exit(EXIT_FAILURE);
	}
}
