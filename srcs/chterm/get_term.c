/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_term.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 09:49:53 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/19 14:19:04 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "chterm.h"
#include <errno.h>

int		ft_get_term(void)
{
	ft_printf("here\n");
	if (tcgetattr(0, &(get_term()->mod)) == -1\
	|| tcgetattr(0, &(get_term()->off)) == -1)
	{
		ft_eprintf("21sh : can't get stat of terminal\n");
		return (-1);
	}
	ft_printf("here\n");
	if (ft_change_term() == -1)
		return (-1);
	if (ft_get_termcap() == -1)
	{
		ft_eprintf("error get termcap\n");
		return (-1);
	}
	return (0);
}
