/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkcmd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 10:07:39 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/20 12:19:09 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checkcmd.h"
#include "ft_printf.h"

char	*ft_get_promt(int missing_value)
{
	if (BACKSLASH == missing_value)
		return ("backslash> ");
	if (DQUOTE == missing_value)
		return ("double quote> ");
	if (QUOTE == missing_value)
		return ("simple quote> ");
	if (BQUOTE == missing_value)
		return ("back quote> ");
	if (PARENTHESIS == missing_value)
		return ("subshl> ");
	if (BRACKET == missing_value)
		return ("bracket> ");
	if (CURLY_BRACKET == missing_value)
		return ("cursh> ");
	return ("> ");
}

int			ft_isinc(char *s)
{
	ft_dprintf(9, "%s\n", s);
	while (*s)
	{
		if (*s == '\\' && *(s + 1) == '\n' && *(s + 2) == '\0')
			return (BACKSLASH);
		if (*s == '"' && ft_dquote(&s))
			return (DQUOTE);
		else if (*s == '\'' && ft_quote(&s))
			return (QUOTE);
		else if (*s == '`' && ft_bquote(&s))
			return (BQUOTE);
		else if (*s == '[' && ft_bracket(&s))
			return (BRACKET);
		else if (*s == '(' && ft_parenthesis(&s))
			return (PARENTHESIS);
		else if (*s == '{' && ft_curly_bracket(&s))
			return (CURLY_BRACKET);
		else
			s++;
	}
	return (0);
}
