/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_quote.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 10:15:12 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/20 11:00:17 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checkcmd.h"

int		ft_dquote(char **s)
{
	char *str;

	str = *s + 1;
	while (*str)
	{
		if (*str == '\\')
			s++;
		else if (*str == '"')
		{
			*s = str;
			return (0);
		}
		str++;
	}
	*s = str;
	return (1);
}

int		ft_quote(char **s)
{
	char *str;

	str = *s + 1;
	while (*str)
	{
		if (*str == '\\')
			str++;
		else if (*str == '\'')
		{
			*s = str;
			return (0);
		}
		str++;
	}
	*s = str;
	return (1);
}

int		ft_bquote(char **s)
{
	char *str;

	str = *s + 1;
	while (*str)
	{
		if (*str == '\\')
			str++;
		else if (*str == '`')
		{
			*s = str;
			return (0);
		}
		str++;
	}
	*s = str;
	return (1);
}

