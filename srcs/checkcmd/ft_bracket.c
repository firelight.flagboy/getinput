/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bracket.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 10:15:32 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/20 11:00:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checkcmd.h"

int		ft_bracket(char **s)
{
	char *str;

	str = *s;
	while (*str)
	{
		if (*str == '\\')
			s++;
		else if (*str == ']')
		{
			*s = str;
			return (0);
		}
		str++;
	}
	*s = str;
	return (1);
}

int		ft_parenthesis(char **s)
{
	char *str;

	str = *s;
	while (*str)
	{
		if (*str == '\\')
			s++;
		else if (*str == ')')
		{
			*s = str;
			return (0);
		}
		str++;
	}
	*s = str;
	return (1);
}

int		ft_curly_bracket(char **s)
{
	char *str;

	str = *s;
	while (*str)
	{
		if (*str == '\\')
			s++;
		else if (*str == '}')
		{
			*s = str;
			return (0);
		}
		str++;
	}
	*s = str;
	return (1);
}

