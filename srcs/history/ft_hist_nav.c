/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hist_nav.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 14:25:45 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/21 11:54:51 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "history.h"
#include "ft_printf.h"

void		ft_move_to_end(int len, t_cursor *cr)
{
	int		i;

	i = -1;
	while (++i < len)
		ft_move_cursor(RIGHT);
	cr->id_list = len;
}

static void		ft_hist_prev(t_hist *node, t_cursor *cr)
{
	t_listd	**head;
	int		i;

	head = &cr->lst;
	i = 0;
	while (node && i < cr->id_hist - 1)
	{
		i++;
		node = node->next;
	}
	if (node == NULL)
		return ;
	cr->id_hist = i;
	i = cr->id_list;
	cr->id_list = 0;
	if (*head)
		ft_listd_free(head);
	if (!(*head = ft_str_to_listd(node->cmd)))
		exit(EXIT_FAILURE);
	cr->l_list = ft_listd_len(*head);
}

static void		ft_hist_next(t_hist *node, t_cursor *cr)
{
	t_listd	**head;
	int		i;

	head = &cr->lst;
	i = 0;
	while (node && i < cr->id_hist + 1)
	{
		i++;
		node = node->next;
	}
	if (node == NULL)
		return ;
	ft_dprintf(9, "next cmd:%s\n", node->cmd);
	cr->id_hist = i;
	i = cr->id_list;
	cr->id_list = 0;
	if (*head)
		ft_listd_free(head);
	if (!(*head = ft_str_to_listd(node->cmd)))
		exit(EXIT_FAILURE);
	cr->l_list = ft_listd_len(*head);
}

void			ft_get_history(int mode, t_cursor *cr)
{
	t_hist	**head;
	t_hist	*node;
	int		i;

	head = get_history();
	if (head == NULL || *head == NULL)
		return ;
	node = *head;
	i = cr->id_list;
	if (mode == PREV && cr->id_hist >= 0)
		ft_hist_prev(node, cr);
	else if (mode == NEXT)
		ft_hist_next(node, cr);
}
