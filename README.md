# getinput
getinput for get the input of the user in terminal
## Command
| control | description | status |
| --- | --- | --- |
| `backspace` | remove the `char` that the `cursor` is on and move to the `left` | implemented |
| `del` | remove the `char` that the `cursor` is on | implemented |
| `left arrow` | move the `cursor` to the left | implemented |
| `right arrow` | move the `cursor` to the right | implemented |
| `shift + left arrow` | move to the previous `word` | implemented |
| `shift + right arrow` | move to the next `word` | implemented |
| `page up` | move to the previous `line` | don't work on multi line |
| `page down` | move to the next `line` | don't work on multi line |
| `home` | move to the begin of `input` | implemented |
| `end` | move to the end of the `input` | implemented |
| `option + v` | paste the content of clipboard to the `inut` | not implemented |
| `option + left arrow` | `copy` the content at the `left` of the cursor | not implemented |
| `option + right arrow` | `copy` the content at the `right` of the cursor | not implemented |
| `option + <` | `cut` the content at the `left` of the `cursor` | not implemented |
| `option + >` | `cut` the content at the `right` of the `cursor` | not implemented |
