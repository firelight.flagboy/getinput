# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    getinput_srcs.mk                                   :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/15 15:15:20 by fbenneto          #+#    #+#              #
#    Updated: 2018/03/20 15:02:01 by fbenneto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# SRC_TEST_DIR=\
	# test_dir/
#
# SRC_TEST_NAME=\
#
# SRC_TEST = $(addprefix $(SRC_TEST_DIR), $(SRC_TEST_NAME))

#######
# Src #
#######

SRC_DIR = ./srcs/

SRC_HISTORY_DIR=\
	history/

SRC_HISTORY_NAME=\
	ft_hist_nav.c\
	ft_hist.c\

SRC_HISTORY = $(addprefix $(SRC_HISTORY_DIR), $(SRC_HISTORY_NAME))

SRC_CHECKCMD_DIR=\
	checkcmd/

SRC_CHECKCMD_NAME=\
	checkcmd.c\
	ft_bracket.c\
	ft_quote.c\

SRC_CHECKCMD = $(addprefix $(SRC_CHECKCMD_DIR), $(SRC_CHECKCMD_NAME))

SRC_LIBFT_DIR=\
	libft/

SRC_LIBFT_NAME=\
	ft_strjoin.c\
	ft_strdel.c\

SRC_LIBFT = $(addprefix $(SRC_LIBFT_DIR), $(SRC_LIBFT_NAME))

SRC_READ_DIR=\
	read/

SRC_READ_NAME=\
	ft_read.c\
	ft_display.c\
	ft_process.c\
	ft_step_word.c\
	ft_process_ascii.c\
	ft_process_key.c\
	ft_process_cursor.c\
	ft_process_hist.c\
	ft_save_cmd.c\

SRC_READ = $(addprefix $(SRC_READ_DIR), $(SRC_READ_NAME))

SRC_CURSOR_DIR=\
	cursor/

SRC_CURSOR_NAME=\
	get_cursor.c\
	ft_move_cursor.c\

SRC_CURSOR = $(addprefix $(SRC_CURSOR_DIR), $(SRC_CURSOR_NAME))

SRC_LISTD_DIR=\
	ft_listd/

SRC_LISTD_NAME=\
	ft_listd_add_at.c\
	ft_listd_add_back.c\
	ft_listd_add_front.c\
	ft_listd_create_elem.c\
	ft_listd_free.c\
	ft_listd_len.c\
	ft_listd_remove_at.c\
	ft_listd_to_str.c\
	ft_str_to_listd.c\

SRC_LISTD = $(addprefix $(SRC_LISTD_DIR), $(SRC_LISTD_NAME))

SRC_SIGNAL_DIR=\
	signal/

SRC_SIGNAL_NAME=\
	setsignal.c\

SRC_SIGNAL = $(addprefix $(SRC_SIGNAL_DIR), $(SRC_SIGNAL_NAME))

SRC_ERRNO_DIR=\
	errno/

SRC_ERRNO_NAME=\
	my_errno_arg.c\
	my_errno_atoi.c\
	my_errno_error.c\
	my_errno_ft_strerror.c\
	my_errno_str.c\
	my_errno_warnx.c\
	my_errno.c\

SRC_ERRNO = $(addprefix $(SRC_ERRNO_DIR), $(SRC_ERRNO_NAME))

SRC_CHTERM_DIR=\
	chterm/

SRC_CHTERM_NAME=\
	chterm_singleton.c\
	change_term.c\
	get_term.c\
	reset_term.c\
	get_termcap.c\
	setterm.c\

SRC_CHTERM = $(addprefix $(SRC_CHTERM_DIR), $(SRC_CHTERM_NAME))

SRC_NAME =\
	$(SRC_ERRNO)\
	$(SRC_CHTERM)\
	$(SRC_SIGNAL)\
	$(SRC_CURSOR)\
	$(SRC_LISTD)\
	$(SRC_LIBFT)\
	$(SRC_READ)\
	$(SRC_CHECKCMD)\
	$(SRC_HISTORY)\
	ft_putchar_ent.c\
	getinput.c\

SRC_DIR_ALL=\
	$(SRC_HISTORY_DIR)\
	$(SRC_CHECKCMD_DIR)\
	$(SRC_LIBFT_DIR)\
	$(SRC_READ_DIR)\
	$(SRC_CURSOR_DIR)\
	$(SRC_LISTD_DIR)\
	$(SRC_SIGNAL_DIR)\
	$(SRC_ERRNO_DIR)\
	$(SRC_CHTERM_DIR)\

SRC = $(addprefix $(SRC_DIR), $(SRC_NAME))

#######
# Obj #
#######

OBJ_DIR = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o)
OBJ_ALL = $(addprefix $(OBJ_DIR), $(OBJ_NAME))
OBJ_DIR_ALL = $(addprefix $(OBJ_DIR), $(SRC_DIR_ALL))
