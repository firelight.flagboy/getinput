# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/15 15:10:52 by fbenneto          #+#    #+#              #
#    Updated: 2018/03/16 15:19:06 by fbenneto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

########
# Name #
########

NAME = getinput
MKNAME = getinput

########################
# include makefile var #
########################

include ./$(MKNAME)_srcs.mk
include ./$(MKNAME)_inc.mk

#############
# ECHO RULE #
#############

ifneq ($(words $(MAKECMDGOALS)),1)
.DEFAULT_GOAL = all
%:
		@$(MAKE) $@ --no-print-directory -rRf $(firstword $(MAKEFILE_LIST))
else
ifndef ECHO
T := $(shell $(MAKE) $(MAKECMDGOALS) --no-print-directory \
		-nrRf $(firstword $(MAKEFILE_LIST)) \
		ECHO="COUNTTHIS" | grep -c "COUNTTHIS")

N := x
C = $(words $N)$(eval N := x $N)
ECHO = printf "\r\e[K[$(MKNAME):\t%3s%%]" "`expr "\`expr $C '*' 100 / $T\`"`"
endif

#########
# MACRO #
#########

NC		= "\\033[0m"
RED		= "\\033[31m"
GREEN	= "\\033[32m"
YELLOW	= "\\033[33m"
BLUE	= "\\033[34m"
MAJENTA	= "\\033[35m"
CYAN	= "\\033[36m"
BOLD	= "\\033[1m"
CHEK	= "✓"
OK		= "$(GREEN)$(CHEK)$(NC)"

############
# COMPILER #
############

CC =clang
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S), Linux)
	CFLAGS += -O3
endif
ifeq ($(UNAME_S), Darwin)
	CFLAGS += -Wall -Werror -Wextra
endif

#########
# RULES #
#########

all : $(NAME)

$(SRC) : $(INCLUDE)

$(NAME) : $(LIB_PRINTF) $(OBJ_DIR) $(OBJ_DIR_ALL) $(OBJ_ALL)
	@$(ECHO)
	@printf " compile binary $(BOLD)$(CYAN)$@$(NC)"
	@$(CC) $(CFLAGS) -ltermcap -o $@ $(OBJ_ALL) $(LIB_PRINTF)
	@printf $(OK)'\n'

$(OBJ_DIR_ALL):
	@$(ECHO)
	@printf " creating folder $(MAJENTA)$(BOLD)$@$(NC)"
	@mkdir -p $@
	@printf ' '$(OK)

$(OBJ_DIR) :
	@$(ECHO)
	@printf " creating folder $(MAJENTA)$(BOLD)$@ dir$(NC) "
	@mkdir -p $@
	@printf $(OK)

$(OBJ_DIR)%.o: $(SRC_DIR)%.c ./Makefile ./getinput_inc.mk ./getinput_srcs.mk
	@$(ECHO)
	@printf " compile $(YELLOW)$(BOLD)$<$(NC)"
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC) $(INC_PRINTF) $(INC_LIBFT)

$(SRC_ALL): $(INCLUDE) $(INCLUDE_PRINTF)

$(LIB_PRINTF) : $(SRCS_PRINTF)
	@make -C $(LIB_PRINTF_DIR) $(LIB_PRINTF_NAME)

clean :
	@make -C $(LIB_PRINTF_DIR) clean
	@$(ECHO)
	@printf " rm all $(BOLD)$(RED)obj file"
	@rm -rf $(OBJ_DIR)
	@printf ' '$(NC)$(OK)'\n'

fclean : clean
	@make -C $(LIB_PRINTF_DIR) naelc
	@$(ECHO)
	@printf " rm $(BOLD)$(CYAN)$(NAME)"
	@rm -f $(NAME)
	@printf ' '$(NC)$(OK)

norme : $(SRC_ALL) $(INCLUDE)
	@make -C $(LIB_PRINTF_DIR) norme
	@printf "[$(MKNAME): $@]\n"
	@norminette $^ | grep -B 1 "Warning\|Error" || true
	@printf "$(GREEN)$(BOLD)DONE$(NC)\n"

proper :
	@make -C ./ all
	@make -C ./ clean

re :
	@make -C ./ fclean
	@make -C ./ all

run :
	@make -C ./ all
	./21sh || true

print-%:
	@echo $($*)

.PHONY: proper re norme all fclean clean naelc run jump libft printf
endif
