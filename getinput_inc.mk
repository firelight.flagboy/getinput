# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    getinput_inc.mk                                    :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/15 15:18:04 by fbenneto          #+#    #+#              #
#    Updated: 2018/03/20 14:50:07 by fbenneto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#############
# Ft_Printf #
#############

LIB_PRINTF_DIR = ./ft_printf/
LIB_PRINTF_NAME = $(shell make -C $(LIB_PRINTF_DIR) print-NAME)
LIB_PRINTF = $(addprefix $(LIB_PRINTF_DIR), $(LIB_PRINTF_NAME))

SRCS_PRINTF_NAME = $(shell make -C $(LIB_PRINTF_DIR) print-SRC)
SRCS_PRINTF = $(addprefix $(LIB_PRINTF_DIR), $(SRCS_PRINTF_NAME))

###########
# Include #
###########

INC_DIR_PRINTF=\
	$(LIB_PRINTF_DIR)includes\

INC_DIR =\
	./inc/

INC_NAME = \
	chterm.h\
	my_errno.h\
	setsignal.h\
	history.h\
	cursor.h\
	libft.h\
	checkcmd.h\
	ft_listd.h\
	getinput.h\

INCLUDE	= $(addprefix $(INC_DIR), $(INC_NAME))

INC_DIR_ALL =\
	$(INC_DIR)\
	$(INC_DIR_PRINTF)\

INC = $(addprefix -I, $(INC_DIR_ALL))
