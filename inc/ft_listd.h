/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 15:06:55 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/16 11:48:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LISTD_H
# define FT_LISTD_H

# include <stdlib.h>
# include <stdint.h>
# include <string.h>

typedef struct		s_listd
{
	struct s_listd	*next;
	struct s_listd	*prev;
	char			c;
}					t_listd;

t_listd				*ft_listd_create_elem(char c);
int					ft_listd_add_front(char c, t_listd **headref);
int					ft_listd_add_back(char c, t_listd **headref);
size_t				ft_listd_len(t_listd *head);
char				*ft_listd_to_str(t_listd **headref);
char				*ft_listd_to_str_r(t_listd **headref);
int					ft_listd_add_at(t_listd **headref, ssize_t index, char c);
void				ft_listd_free(t_listd **headref);
void				ft_listd_remove_at(t_listd **headref, ssize_t index);
t_listd				*ft_str_to_listd(char const *s);
#endif
