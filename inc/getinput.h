/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getinput.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 16:27:16 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/20 15:07:25 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GETINPUT_H
# define GETINPUT_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <string.h>
# include <stdint.h>
# include <signal.h>
# include <errno.h>

# include "ft_printf.h"
# include "chterm.h"
# include "my_errno.h"
# include "setsignal.h"
# include "cursor.h"
# include "libft.h"
# include "history.h"
# include "checkcmd.h"

void		ft_head_promt(void);
char		*ft_read(char *pt);
int			ft_step_word(int move, t_cursor *cr);
int			ft_process(t_cursor *cr, char *s, int rt);
int			ft_process_ascii(t_cursor *cr, char s);
int			ft_process_key(t_cursor *cr, char *s);
int			ft_process_cursor(t_cursor *cr, int	mode);
void		ft_display(t_cursor *cr);
int			ft_process_hist(t_cursor *cr, int md);
int			ft_save_cmd(t_cursor *cr);
int			ft_reset_cmd(t_cursor *cr);
#endif
