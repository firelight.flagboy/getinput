/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 14:22:26 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/20 14:28:35 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HISTORY_H
# define HISTORY_H

# include <stdlib.h>
# include "cursor.h"
# include "ft_listd.h"
// # include "libft.h"

typedef struct		s_hist
{
	char			*cmd;
	int				index;
	struct s_hist	*next;
	struct s_hist	*prev;
}					t_hist;

t_hist				**get_history(void);
t_hist				*ft_hist_creat_elem(char *s);
int					ft_hist_push(t_hist **headref, char *cmd);
void				ft_hist_free(t_hist **headref);
void				ft_get_history(int mode, t_cursor *cr);
#endif
