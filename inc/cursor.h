/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 13:42:30 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/20 15:06:17 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CURSOR_H
# define CURSOR_H

# include "ft_listd.h"

typedef struct	s_cursor
{
	int			r;
	int			l;
	int			l_with;
	int			r_with;
	int			r_max;
	int			l_max;
	int			id_list;
	int			l_list;
	int			id_hist;
	int			l_pt;
	char		*res;
	t_listd		*lst;
	char		*exit;
	char		*pt;
	char		*scmd;
}				t_cursor;

enum	e_move
{
	NOP,
	LEFT,
	RIGHT,
	UP,
	DOWN,
	NEXT,
	PREV
};

t_cursor	*get_cursor(void);
int			ft_move_cursor_multi(int move);
int			ft_move_cursor(int move);
#endif
