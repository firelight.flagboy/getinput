/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chterm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 09:28:03 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/19 15:18:46 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHTERM_H
# define CHTERM_H

# include <term.h>
# include "cursor.h"
# include "ft_printf.h"

typedef struct termios		t_termios;

typedef struct	s_keystrock
{
	char		*right_a;
	char		*upper_a;
	char		*down_a;
	char		*left_a;
	char		*del;
	char		*home;
	char		*end;
	char		*p_up;
	char		*p_do;
	char		*opl;
	char		*opr;
	char		*sil;
	char		*sir;
}				t_ks;

typedef struct	s_term
{
	t_termios off;
	t_termios mod;
}				t_term;

void		ft_display(t_cursor *cr);
t_term		*get_term(void);
t_ks		*get_ks(void);
int			ft_putchar_ent(int c);
int			ft_change_term(void);
int			ft_get_term(void);
void		ft_reset_term(void);
int			ft_get_termcap(void);
void		ft_setterm(char *term);
#endif
