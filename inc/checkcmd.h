/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkcmd.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 10:07:59 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/20 10:24:54 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKCMD_H
# define CHECKCMD_H

# include <stdlib.h>

enum e_missing_type
{
	NOT,
	BACKSLASH,
	DQUOTE,
	QUOTE,
	BQUOTE,
	PARENTHESIS,
	BRACKET,
	CURLY_BRACKET
};

int		ft_dquote(char **s);
int		ft_quote(char **s);
int		ft_bquote(char **s);
int		ft_bracket(char **s);
int		ft_parenthesis(char **s);
int		ft_curly_bracket(char **s);
char	*ft_get_promt(int missing_value);
int		ft_isinc(char *s);
#endif
