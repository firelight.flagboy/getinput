/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setsignal.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 11:38:21 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/20 12:13:39 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SETSIGNAL_H
# define SETSIGNAL_H

# include <signal.h>
# include <stdlib.h>
# include <sys/ioctl.h>
# include <unistd.h>
# include "chterm.h"
# include "ft_printf.h"
# include "libft.h"
# include "cursor.h"

# ifndef NB_SIGNAL
#  define NB_SIGNAL 3
# endif

pid_t	g_pid;
typedef struct ttysize		t_ttysize;
typedef struct sigaction 	t_siga;

void	handler_resize(int sig);
void	handler_pid(int sig);
void	handler_promt(int sig);
void	handler_quit(int sig);

int		ft_setsignal(void);
void	ft_head_promt(void);
void	ft_place_cursor(t_cursor *cursor);
#endif
